<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'hostvw_szcz');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'i8xqnnadsv4a8knbxjowsswhgomx8r8yygrjaseasww8lmowywwx21khr8hhqqpx');
define('SECURE_AUTH_KEY',  '5rfdjexh85xrli4lmbtu7ufrh0lwhctixcnggy3kvhqsojg9qeuv9jrt24urupcj');
define('LOGGED_IN_KEY',    '6cgk7plxhlmcswbpu4u3tynbu9ftxyvi5kplkseol0rwxay0sko8gpb0wrwwtwmt');
define('NONCE_KEY',        '9qtojep1davrhlp83upjf2umee5fi4c7hxeggm3my2wsoqjbpbx1oneagus6oz7c');
define('AUTH_SALT',        'vlhrxl2cnpwz9g9arid5iyy3t1rp2iorjo2a57hq65yrjaho7u1ptixiggomzk0i');
define('SECURE_AUTH_SALT', 'liw04ljecimorpl6t1ky4cw7jetrxidcuajg6g16ic3hfrqgtfgzneuxbhhjfa0y');
define('LOGGED_IN_SALT',   'yox3hi2cyafdtii93ldy7kmdzjmpyte9lezwj7tiwhtwyda0nm97zpn8uyejqu8z');
define('NONCE_SALT',       '9xpbx3rqvsf1zdmgwjyi9k0wtbeu1sce94we7zzacinbxvljy8rfwsfjzbugbyt1');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
